// "Quiet Coins" by Ryan Pavlik
// Copyright 2020, Ryan Pavlik <ryan.pavlik@gmail.com>
// SPDX-License-Identifier: CC-BY-4.0

// This is the main file, for blank coins/tokens.


// Outer bounds: based on rough dimensions of checkers
maxDiameter = 31.5;
maxThickness = 7;

// Height of bottom step
stepHeight = 2;

// Inside ledge dimensions, for supporting next in stack
ledgeWidth = 1.6;
ledgeHeight = 1;

// inside space allowed for raised designs
insideDesignHeight = 2;
flareAngle = 30;

//  Computed values
flareWidth = (maxThickness -  stepHeight) * tan(flareAngle);
baseDiameter = maxDiameter - 2 * flareWidth;

// inside diameter allowed for designs
insideDesignDiameter = baseDiameter - ledgeWidth;

aLittleExtra=0.1;

$fn = 100;

module baseToken() {
    // Translate down so the "work surface" is at 0
    translate([0,0, -1 * (maxThickness - ledgeHeight - insideDesignHeight)]) {
        difference() {
            // Create overall shape
            union() {
                // base
                cylinder(stepHeight, d=baseDiameter);
                // tapered part
                translate([0,0,stepHeight]) {
                    cylinder(maxThickness - stepHeight, d1=baseDiameter, d2=maxDiameter);
                }
            };
            // Cut out top cylinder: creates ledge
            translate([0,0, maxThickness - ledgeHeight]) {
                cylinder(ledgeHeight+aLittleExtra, d=baseDiameter + ledgeWidth);
            };
            // Cut out inner cylinder: creates inset surface
            translate([0,0, maxThickness - ledgeHeight - insideDesignHeight]) {
                cylinder(insideDesignHeight+aLittleExtra, d=insideDesignDiameter);
            };
        }
    }
}

