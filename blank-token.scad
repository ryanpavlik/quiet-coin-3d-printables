// "Quiet Coins" by Ryan Pavlik
// Copyright 2020, Ryan Pavlik <ryan.pavlik@gmail.com>
// SPDX-License-Identifier: CC-BY-4.0

// This is a minimal file for exporting a blank coin

include <token.scad>;

baseToken();
