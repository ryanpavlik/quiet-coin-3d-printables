// "Quiet Coins" by Ryan Pavlik
// Copyright 2020, Ryan Pavlik
// SPDX-License-Identifier: CC-BY-4.0

// This file produces "quiet coins" with numbers and dots on them,
// as a sample adaptation.
include <token.scad>;

module numberToken(num) {
    pipDiameter = 2;
    
    pipInset = 3 + pipDiameter/2;
    fontHeight = 15;
    designHeight = insideDesignHeight *0.75;
    union() {
        baseToken();
        linear_extrude(height=designHeight) {
        text(str(num), fontHeight, halign="center", valign="center");
        }
        for (i = [1 : num]) {
            rotate([0, 0, (360/num) * i]) {
                translate([(insideDesignDiameter - pipInset)/2, 0, 0]) {
                    cylinder(insideDesignHeight, d=pipDiameter);
                }
            }
        }
        
    }
}

// numberToken(2);

// This creates a 3x3 grid, containing the digits 1-9
spacing = maxDiameter + 3;
for (i = [0 : 2]) {
    for (j = [0:2]) {
        translate([ spacing * i, spacing * j, 0])
            numberToken(i * 3 + j + 1);
    }
}
